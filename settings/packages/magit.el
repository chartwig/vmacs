;; Magit configuration -*- lexical-binding: t; -*-
(use-package magit
  :ensure t
  :bind
  ("C-x g" . magit-status)
  ("C-x M-g" . magit-dispatch-popup))

(use-package evil-magit
  :ensure t
  :after (evil magit))

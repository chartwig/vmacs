;; Powerline configuration -*- lexical-binding: t; -*-
(use-package powerline
  :ensure t
  :config
  (powerline-default-theme))

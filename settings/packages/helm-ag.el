;; Helm Ag configuration -*- lexical-binding: t; -*-
(use-package helm-ag
  :ensure t
  :after (helm))


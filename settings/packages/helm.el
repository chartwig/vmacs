;; Helm configuration -*- lexical-binding: t; -*-
(use-package helm
  :ensure t
  :config
  (helm-mode 1)
  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action))

(define-key my-leader-map (kbd "SPC") 'helm-M-x)
(define-key my-leader-map "b" 'helm-buffers-list)
(define-key my-leader-map "f" 'helm-find-files)
(define-key my-leader-map "h" 'helm-command-prefix)


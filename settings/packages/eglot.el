;; Eglot configuration -*- lexical-binding: t; -*-

(use-package eglot
  :ensure t
  :after (hydra)
  :config (defhydra hydra-eglot ()
    "Eglot Menu"
    ("e" eglot "eglot: Run LSP Server")
    ("c" eglot-reconnect "eglot-reconnect: Recconect to LSP Server")
    ("q" eqlot-shutdown "eglot-shutdown: Shutdown LSP Server")
    ("r" eglot-rename "eglot-rename: Rename the symbol at point")
    ("f" eglot-format "eglot-format: Format the buffer or region")
    ("a" eglot-code-actions "eglot-code-actions: Display code actions")
    ("h" eglot-help-at-point "eglot-help-at-point: Display code help")
    ("v" eglot-events-buffer "eglot-events-buffer: Display the events buffer")
    ("s" eglot-stderr-buffer "eglot-stderr-buffer: Display the stderr buffer"))
    (define-key my-leader-map "e" 'hydra-eglot/body))



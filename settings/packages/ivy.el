;; Ivy configuration -*- lexical-binding: t; -*-
(use-package ivy
  :ensure t
  :after flx
  :config
  (ivy-mode 1)
  (setq ivy-re-builders-alist '((t . ivy--regex-fuzzy))))

(use-package counsel
  :ensure t
  :after ivy)

(define-key my-leader-map (kbd "SPC") 'counsel-M-x)
(define-key my-leader-map "f" 'counsel-find-file)



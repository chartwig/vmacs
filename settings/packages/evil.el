;; Evil configuration -*- lexical-binding: t; -*-
(use-package evil
  :ensure t
  :init
  (setq evil-want-integration nil)
  :config
  ;; Set up key chord mode here, as this is about key bindings too
  (key-chord-mode 1)
  (evil-mode 1)
  ;; Variable used for all leader key bindings.
  (defvar my-leader-map (make-sparse-keymap)
    "Keymap for \"leader key\" shortcuts.")

  (define-key evil-normal-state-map (kbd "SPC") my-leader-map)
  (key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
  (key-chord-define global-map "jk" 'keyboard-escape-quit))

(use-package evil-collection
  :ensure t
  :after evil
  :config (evil-collection-init))

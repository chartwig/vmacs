;; Built-in Emacs Window configuration -*- lexical-binding: t; -*-

(defhydra hydra-window ()
    "Window Menu"
    ("f" toggle-frame-fullscreen "toggle-frame-fullscreen: Toggle Fullscreen Mode"))
    (define-key my-leader-map "w" 'hydra-window/body)

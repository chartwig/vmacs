;; Editor Settings -*- lexical-binding: t; -*-

;; Turn off suto-save, the visible-bell and the startup message
(setq backup-inhibited 't
      auto-save-default 'nil
      visible-bell 'nil
      inhibit-startup-message 't
      inhibit-startup-echo-area-message '"")

;; Get rid of the distracting toolbar and scrollbar
(customize-set-variable 'menu-bar-mode nil)
(tool-bar-mode -1)
(toggle-scroll-bar -1)
(toggle-frame-fullscreen)

;; Add some line numbers
(global-linum-mode 1)

;; Add column numbers
(column-number-mode 1)

;; Add matching parentheses highlighting
(setq show-paren-delay 0)
(show-paren-mode 1)

;; highlight the current line
;; but only if it is not the char terminal
(add-hook 'after-change-major-mode-hook
          '(lambda () (hl-line-mode (if (equal major-mode 'term-mode) 0 1))))

;; Display the file path for the buffer name
;; TODO: Somehow only show from the point of the project root using projectile, if in a project...
;; TODO: Replace my home path with ~
;;(setq-default mode-line-buffer-identification
;;            (cons (car mode-line-buffer-identification) '(buffer-file-name)))

;; How about some config settings?
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

;; Add a bit more space and a vertical line to my line number section
(setq linum-format "%4d")

;;; Text Settings ;;;

;; Turn off tab characters and use spaces instead.
(setq-default indent-tabs-mode nil)

;; Make sure we have LF endings
(setq-default buffer-file-coding-system 'utf-8-unix)

;; warn only when opening files bigger than 100MB
(setq large-file-warning-threshold 100000000)

;; Make files that are 100mb+ easier to work with
(defun my-find-file-check-make-large-file-read-only-hook ()
  "If a file is over a given size, make the buffer read only."
  (when (> (buffer-size) (* 1024 1024 100))
    (fundamental-mode)))

(add-hook 'find-file-hook 'my-find-file-check-make-large-file-read-only-hook)

;; Use a sweet font
(add-to-list 'default-frame-alist '(font . "Fira Code"))

(set-face-attribute 'default nil :height 90)

;; Set up language specific settings. Comment out any that you do not want.
;; Loads language setting files in settings/languages/<name in the list below>.el. So, if you want to add a
;; new language settings file, say for helm, add a helm.el file to settings/languages in the emacs.d directory,
;; and then add "helm" to the list parameter for vmacs-load-settings-files.
(vmacs-load-setting-files "languages" '("csharp"
                                        "css"
                                        "html"
                                        "javascript"
                                        "markdown"
                                        "org"
                                        "python"
                                        "typescript"
                                        "vb"
                                        "xml"))

;; Set up emacs specific settings. This will hold all other settings for things like shells, buffers, line numbers, highlighting, etc.
(vmacs-load-setting-files "emacs" '("buffer"
                                    "window"))

;; Typescript Editor Settings -*- lexical-binding: t; -*-

(use-package typescript-mode
  :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-mode))
  (font-lock-add-keywords 'typescript-mode
                          '(
                            ("^@[^(]*" (0 font-lock-preprocessor-face))
                            ("[a-zA-Z]*:" (0 font-lock-constant-face))
                            )))


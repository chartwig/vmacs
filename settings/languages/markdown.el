;; Markdown Editor Settings -*- lexical-binding: t; -*-

(use-package markdown-mode
  :ensure t)

(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))

;; Html Editor Settings -*- lexical-binding: t; -*-

(use-package web-mode
  :ensure t
  :config
  ;; Set HTML indentation to 2 spaces
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-indent-style 2)

  ;; Highlight matching elements
  (setq web-mode-enable-current-element-highlight t)

  (add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode)))


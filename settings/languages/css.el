;; CSS/Sass/Less Editor Settings -*- lexical-binding: t; -*-

(add-to-list 'auto-mode-alist '("\\.css\\'" . css-mode))
(add-to-list 'auto-mode-alist '("\\.less\\'" . less-css-mode))
(add-to-list 'auto-mode-alist '("\\.scss\\'" . less-css-mode))

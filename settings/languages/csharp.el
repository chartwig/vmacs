;; CSharp Editor Settings -*- lexical-binding: t; -*-

(use-package csharp-mode
  :ensure t
  :config (add-to-list 'auto-mode-alist '("\\.cs\\'" . csharp-mode)))


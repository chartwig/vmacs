;; VMacs Configuration -*- lexical-binding: t; -*-

;; Set utf-8 as default coding system
;; This needs to happen before loading code files, so it cannot
;; be placed into the editor settings, to my chagrin...
(prefer-coding-system 'utf-8)
(setq coding-system-for-read 'utf-8)
(setq coding-system-for-write 'utf-8)

(require 'package)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/"))
(add-to-list 'package-archives
             '("gnu" . "http://elpa.gnu.org/packages/"))

(package-initialize)
(package-refresh-contents)
;; Load Use Package and Dep. ;;

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))
(require 'bind-key)

;; Put all of the emacs generated garbage elsewhere.
(setq-default custom-file (expand-file-name ".custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; Load all of my personal lisp files - if you want to add more functionality
;; outside of a traditional config setting for a package, or want to put a
;; gist/snippet/random lisp file from the wiki somewhere, this will do well.
(mapc 'load (file-expand-wildcards "~/.emacs.d/lisp/*.el"))

;;; Package Settings ;;;

(use-package dracula-theme
  :ensure t)

;; Load Package Settings
;; Loads package setting files in settings/packages/<name in the list below>.el. So, if you want to add a
;; new package settings file, say for helm, add a helm.el file to settings/packages in the emacs.d directory,
;; and then add "helm" to the list parameter for vmacs-load-settings-files.
(vmacs-load-setting-files "packages" '("company"
                                       "evil"
                                       "eglot"
                                       "flx"
                                       "hydra"
                                       "ivy"
                                       "magit"
                                       "powerline"
                                       "projectile"))


(use-package smex
  :ensure t)

;;; Emacs Settings ;;;
;; Load settings files, which are top level, so we will send "" for the sub directory
;; Editor is the standard settings file, while custom-editor will allow you to add
;; your own settings without them being wiped by a `git pull`
(vmacs-load-setting-files "" '("editor" "custom-editor"))

